import router from '@system.router';
export default {
    data: {
        title: "奖品列表",
        //1.1添加项目时存放奖品名
        name: "",
    },

    //2.返回主界面
    gotoMainPage() {
        //router.back();
        //2.1页面路由清理
        //这里不能使用back,目前的API中如果需要在界面跳转时携带参数,只能使用push
        //但是一直push就需要使用clear防止一直压栈,clear带来的影响就是每次都需要重新加载界面
        //由于index中的canvas是通过onShow加载的,虽然每次调出转盘时,会出现卡顿现象(转盘绘制慢)
        router.clear();
        router.push ({
            uri: 'pages/index/index',
            params: {
                infoArr: this.infoArr,
            }
        });
    },

    //2.奖项内容编辑
    changeEvent(name, e) {
        //2.1模拟器中需要屏蔽这个函数,远程模拟器中运行正常
        //在模拟器会在onShow中,即使没有对内容进行编辑,也会调用input中的onchange事件,感觉是bug
//        let num = 0;
//        for(var i of this.infoArr) {
//            if (i.name == name) {
//                this.infoArr[num].name = e.value;
//                return;
//            }
//            num++;
//        }
    },

    //3.删除奖项
    deleteEvent: function (name) {
        console.log("delete name is " + name);
        //3.1通过name绑定各个删除按钮与其对应的奖项
        let num = 0;
        for(var i of this.infoArr) {
            if (i.name == name) {
                this.infoArr.splice(num,1);
                return;
            }
            num++;
        }
    },

    //4.弹出添加奖项对话框
    add: function () {
        this.$element('dialog').show();
    },

    //5.点击取消,对话框关闭
    cancel: function () {
        this.$element('dialog').close();
    },

    //6.点击确定,如果名字不为空,添加对应奖项,同时关闭对话框
    ok: function () {
        if (this.name !== '') {
            // 6.1array不支持insert,为了插入到倒数第二的位置,先删除末尾的未中奖,再添加奖项、未中奖
            this.infoArr.pop();
            this.infoArr.push({name:this.name});
            this.infoArr.push({name:"未中奖"});
        }
        this.$element('dialog').close()
    },

    //7.对话框输入内容改变时,保存奖项名字
    getName(e) {
        this.name = e.value;
    },
}
