import prompt from '@system.prompt';
import router from '@system.router';

export default {
    data: {
        //1.1创建奖项信息
        infoArr: [
            { name: '1号奖品' },
            { name: '2号奖品' },
            { name: '3号奖品' },
            { name: '4号奖品' },
            { name: '5号奖品' },
            { name: '6号奖品' },
            { name: '未中奖'  },
        ],
        //1.2画布大小
        circleHeight: 400,
        circleWidth: 360,
        //1.3扇区弧度
        arcAngle: 0,
        //1.4扇区角度
        jiaoDu: 0,
        //1.4动画参数
        animation: '',
        options: {
            duration: 5000,
            fill: 'forwards',
            easing: 'cubic-bezier(.2,.93,.43,1);',
        },
    },

    onShow() {
        const ca = this.$element('canvas');
        const ctx = ca.getContext('2d');

        //2.设定参数
        //2.1定义圆心,显示在画布中间
        var x0 = this.circleWidth * 0.5;
        var y0 = this.circleHeight * 0.5;
        //2.2定义半径
        var radius = this.circleWidth * 0.5;
        //2.3扇形弧度
        this.arcAngle = 360 / this.infoArr.length * Math.PI / 180;
        //2.4扇区角度
        this.jiaoDu = 360 / this.infoArr.length;
        //2.5定义起始弧度,箭头向上,初始度数需要-90deg
        var beginAngle = this.arcAngle * 0.5 - 90 * Math.PI / 180;

        //3.遍历,绘制扇区
        for (var i = 0; i < this.infoArr.length; i++) {
            //3.1结束弧度
            var endAngle = beginAngle + this.arcAngle;
            //3.2开启路径
            ctx.beginPath();
            //3.3起点
            ctx.moveTo(x0, y0);
            //3.4绘制扇区
            ctx.arc(x0, y0, radius, beginAngle, endAngle);
            //3.5设置颜色
            if (i == this.infoArr.length - 1) {
                ctx.fillStyle = '#2f4f4f'; //未中奖灰色
            } else if (i % 2) {
                ctx.fillStyle = '#ffa500';
            } else {
                ctx.fillStyle = '#ff4500';
            }
            //3.6填充颜色
            ctx.fill();

            //4.绘制文字
            //4.1文字弧度
            var textAngle = beginAngle + this.arcAngle * 0.5;
            var text = this.infoArr[i].name;
            //4.2文字坐标
            var textX = x0 + (radius * 2 / 3) * Math.cos(textAngle);
            var textY = y0 + (radius * 2 / 3) * Math.sin(textAngle);
            //4.3平移画布起点到文字位置
            ctx.translate(textX, textY);
            //4.4旋转画布
            ctx.rotate((this.jiaoDu * (i + 1) - 90) * Math.PI / 180);
            //4.5设置文字字号和字体
            ctx.font = "25px '微软雅黑'";
            //4.6文字居中对齐
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            //4.7绘制文字
            ctx.strokeText(text, 0, 0);
            //4.8还原旋转、平移,方便下次旋转
            ctx.rotate(-(this.jiaoDu * (i + 1) - 90) * Math.PI / 180);
            ctx.translate(-textX, -textY);

            //5.更新起始弧度, 将当前扇形的结束弧度作为下一个扇形的起始弧度
            beginAngle = endAngle;
        }
    },

    start: function () {
        //6.旋转事件
        //6.1奖品总数
        let count = this.infoArr.length;
        //6.2生成随机数
        let randomNum = Math.floor(Math.random() * count);
        //6.3转动角度(+ 360*3)
        let deg = randomNum * this.jiaoDu + 360 * 3 + "deg";
        //6.4奖品名
        let index = count - randomNum - 1;
        let name = this.infoArr[index].name;
        console.log("name == " + name);
        //6.5动画帧
        var frames = [
            {
                transform: {
                    rotate: '0deg'
                },
            },
            {
                transform: {
                    rotate: deg
                },
            }
        ];
        //6.5动画绑定
        this.animation = this.$element('canvas').animate(frames, this.options);
        //6.6添加完成事件
        this.animation.onfinish = function () {
            if (randomNum % count) {
                prompt.showDialog({
                    message: "恭喜抽中" + name + "!",
                    duration: 3000,
                });
            } else {
                prompt.showDialog({
                    message: "下次再来！",
                    duration: 3000,
                });
            }
        };
        //6.7调用播放开始的方法
        this.animation.play();
    },

    menu: function () {
        router.clear();
        router.push ({
            uri: 'pages/menuPage/menuPage',
            params: {
                infoArr: this.infoArr,
            }
        });
    },
}