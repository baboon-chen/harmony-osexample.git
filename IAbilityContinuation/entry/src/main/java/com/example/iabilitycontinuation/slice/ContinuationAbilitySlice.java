package com.example.iabilitycontinuation.slice;

import com.example.iabilitycontinuation.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class ContinuationAbilitySlice extends AbilitySlice implements IAbilityContinuation {
    private static final String TAG = ContinuationAbilitySlice.class.getSimpleName();

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, TAG);

    private static final String QUESTION_KEY = "QUESTION_KEY";
    private static final String ANSWER_KEY = "ANSWER_KEY";

    private String questionText;
    private String answerText;

    private TextField questionTextField;
    private TextField answerTextField;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_continuation);
        initComponents();
    }

    private void initComponents() {
        questionTextField = (TextField) findComponentById(ResourceTable.Id_question_content);

        answerTextField = (TextField) findComponentById(ResourceTable.Id_answer_content);

        findComponentById(ResourceTable.Id_send_button).setClickedListener(this::migrateAbility);
        findComponentById(ResourceTable.Id_return_button).setClickedListener(component->terminate());
    }

    @Override
    protected void onActive() {
        super.onActive();

        if (questionText != null) {
            questionTextField.setText(questionText);
        }

        if (answerText != null) {
            answerTextField.setText(answerText);
        }
    }

    private void migrateAbility(Component component) {
        String questionSend = questionTextField.getText();
        String answerSend = answerTextField.getText();
        if (questionSend.isEmpty() && answerSend.isEmpty()) {
            new ToastDialog(this).setText("Text can not be null").show();
            return;
        }

        try {
            continueAbility();
        } catch (IllegalStateException illegalStateException) {
            HiLog.error(LABEL_LOG, "%{public}s", "migrateAbility: IllegalStateException");
        }
    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        intentParams.setParam(QUESTION_KEY, questionTextField.getText());
        intentParams.setParam(ANSWER_KEY, answerTextField.getText());

        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        if (intentParams.getParam(QUESTION_KEY) instanceof String) {
            questionText = (String) intentParams.getParam(QUESTION_KEY);
        }

        if (intentParams.getParam(ANSWER_KEY) instanceof String) {
            answerText = (String) intentParams.getParam(ANSWER_KEY);
        }

        if (!questionText.isEmpty() && ! answerText.isEmpty()) {
            AskRecordSlice.UpdateContent("Q:" + questionText + "\n");
            AskRecordSlice.UpdateContent("A:" + answerText + "\n");
        }

        return true;
    }

    @Override
    public void onCompleteContinuation(int code) {
        questionText = questionTextField.getText();
        answerText = answerTextField.getText();
        if (!questionText.isEmpty() && ! answerText.isEmpty()) {
            AskRecordSlice.UpdateContent("Q:" + questionText + "\n");
            AskRecordSlice.UpdateContent("A:" + answerText + "\n");
        }
    }
}
