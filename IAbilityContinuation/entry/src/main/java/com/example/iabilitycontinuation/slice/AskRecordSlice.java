package com.example.iabilitycontinuation.slice;

import com.example.iabilitycontinuation.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class AskRecordSlice extends AbilitySlice {

    private static String content = "";
    private Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ask_record);
        initComponents();
    }

    private void initComponents() {
        findComponentById(ResourceTable.Id_return_button).setClickedListener(component -> terminate());
        text = (Text) findComponentById(ResourceTable.Id_text);
    }

    @Override
    protected void onActive() {
        super.onActive();
        text.setText(content);
    }

    public static void UpdateContent(String text) {
        content += text;
    }

}
