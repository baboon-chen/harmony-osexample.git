package com.example.iabilitycontinuation.slice;

import com.example.iabilitycontinuation.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

public class MainAbilitySlice extends AbilitySlice {
    private static final String BUNDLE_NAME = "com.example.iabilitycontinuation";

    private static final String CONTINUATION_ABILITY = "com.example.iabilitycontinuation.ContinuationAbility";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initComponents();
    }

    private void initComponents() {
        findComponentById(ResourceTable.Id_first_button).setClickedListener(
                listen->present(new AskRecordSlice(), new Intent()));
        findComponentById(ResourceTable.Id_second_button).setClickedListener(
                component -> startAbility(CONTINUATION_ABILITY));
    }

    private void startAbility(String abilityName) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().withDeviceId("")
                .withBundleName(BUNDLE_NAME)
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
